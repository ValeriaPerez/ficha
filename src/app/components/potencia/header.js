import React, {Component} from 'react';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import {browserHistory} from 'react-router';

const styles = {
  field: {
    marginRight: '20px'
  },
  nav: {}
};

const subjects = [
  [
    <MenuItem key={1} value={1} primaryText="1ro" />,
    <MenuItem key={2} value={2} primaryText="2do" />,
    <MenuItem key={3} value={3} primaryText="3ro" />,
    <MenuItem key={4} value={4} primaryText="4to" />,
    <MenuItem key={5} value={5} primaryText="5to" />,
    <MenuItem key={6} value={6} primaryText="6to" />,
  ],
  [
    <MenuItem key={1} value={1} primaryText="1ro" />,
    <MenuItem key={2} value={2} primaryText="2do" />,
    <MenuItem key={3} value={3} primaryText="3ro" />,
  ]
];

export class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      grade: null,
      subject: null
    };

    this.handleChangeGrade = this.handleChangeGrade.bind(this);
    this.handleChangeSubject = this.handleChangeSubject.bind(this);
  }

  handleChangeGrade(event, index, value) {
    this.setState({
      grade: value
    });
  }

  handleChangeSubject(event, index, value) {
    this.setState({
      subject: value
    });
    if(this.state.grade === 1) {
      if(value === 1) {
        browserHistory.push('/teachers/sec1');
      } else if(value === 2) {
        browserHistory.push('/teachers/sec2');
      } else if(value === 3) {
        browserHistory.push('/teachers/sec3');
      }
    }
  }

  render() {

    let style = {}
    let avatar = '/assets/images/potencia/temporary/avatar.jpg';
    let title = 'Primaria';
    let query = this.props.query;
    if(query && query.level && query.level === 'Secundaria') {
      title = 'Secundaria';
      style.backgroundColor = '#700018';
    }
    return (
      <nav className="main-nav" style={style}>
        <div className="nav-wrapper">
          <div>
            <div className="elements">
              <IconButton tooltip="Potencia Educativa" onClick={this.props.hide}>
                <i className="fa fa-bars" aria-hidden="true"></i>
              </IconButton>
              <h1 className="logo">
                <img src="/assets/images/potencia/potencia.png" />
              </h1>
              <h3>&nbsp;</h3>
            </div>
          </div>
          <div>
            <div className="elements">
              {}
              <button className="nav-buttons">
                <i className="material-icons"></i>
                <span></span>
              </button>
              <button className="nav-buttons">
                <i className="" aria-hidden="true"></i>
                <span></span>
              </button>
              <Avatar src={avatar} />
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  query: React.PropTypes.object
}
