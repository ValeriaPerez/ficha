import React, {Component} from 'react';
import {List, ListItem} from 'material-ui/List';
import {Link} from 'react-router';

export class SidebarMenu extends Component {
  render() {

    return (
      <List className={this.props.hide?"closed":""}>
        <ListItem
          primaryText={this.props.hide?" ":"Asignaturas"}
          containerElement={<Link to="/"/>}
          leftIcon={<i className="fa fa-home" aria-hidden="true"></i>}
          innerDivStyle={this.props.hide?{margin:"0 0 16px 0"}:{}}
        />
        </List>
    );
  }
}
