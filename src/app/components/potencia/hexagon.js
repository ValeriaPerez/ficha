import React, {Component} from 'react';
import classNames from 'classnames';
import {browserHistory} from 'react-router';

import './hexagon.scss';

export class Hexagon extends Component {

  constructor(props) {
    super(props);
    this.handleSendTo = this.handleSendTo.bind(this);
  }

  handleSendTo() {
    if(this.props.index === 0){
      if(typeof this.props.location !== 'undefined'){
        browserHistory.push(`/${this.props.location.split('/')[1]}/activities`);
        return;
      }
      browserHistory.push('/teachers/activities');
    }
  }

  render() {
    let hexClass = classNames('hexagon2', 'cursor-pointer', this.props.anotherClass);
    return (
      <div className={hexClass} style={{backgroundImage: this.props.backgroundImage}} onClick={this.handleSendTo}>
        <div className="hexLeft"></div>
        <div className="hexRight"></div>
        {this.props.children}
      </div>
    );
  }
}

Hexagon.propTypes = {
  backgroundImage: React.PropTypes.string,
  anotherClass: React.PropTypes.string,
  index: React.PropTypes.number,
  location: React.PropTypes.string
};
