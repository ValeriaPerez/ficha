import React,{Component} from 'react';
import classNames from 'classnames';

class SimpleVideo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  render() {
    return (
      <div>
        <div className="rectangle-style bar-grey">
          <div className="box-config">
            <i className="fa fa-lightbulb-o" aria-hidden="true"></i>
          </div>
          <div className="grey-basis">{this.props.name}</div>
          <div className="toggle-info" onClick={() => this.setState({open: !this.state.open})}>
            <i
            className={classNames('fa', 'bar-icon', this.state.open?'fa-caret-up':'fa-caret-down')}
            aria-hidden="true"
            >
            </i>
          </div>
        </div>
        {this.state.open ? (<div className="video-config">
          <iframe className="video-position" width="80%" height="500" src={`https://www.youtube.com/embed/${this.props.id}`} frameBorder="0" allowFullScreen></iframe>
        </div>) : null}
      </div>
    );
  }
}

export default SimpleVideo;
