import React, {Component, PropTypes} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import classNames from 'classnames';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

class Create extends Component {
  constructor(props) {
    super(props);
    this.goActivity = this.goActivity.bind(this);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  goActivity() {
    browserHistory.push('/teachers/terracota');
  }

  render() {
    const content = this.props.createContent;
    return (
      <div>
        <div className="paragraph-style">
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_student)}></div>
        </div>
        <div className="grey-square">
          <h3 className="title-margin">Crea</h3>
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_teacher)}></div>
        </div>
        <div className="yellow-square">
          <img src="assets/images/temporary/icon.png" className="img-icon" alt="ejemplo" />
          <h4 className="title-mg">MÁS IDEAS EN CONTENIDO SOLO PARA WEB</h4>
          {content.content ? content.content.map((embed, index) => {
            return(
              <div key={index}>
                <div dangerouslySetInnerHTML={this.createMarkup(embed.idea_additional_text)}></div>
                <h4>ARCHIVOS ADJUNTOS</h4>
                <ul>
                  <li>
                    <a target="_blank" href={embed.idea_additional_files}>{embed.idea_additional_files}</a>
                  </li>
                </ul>
              </div>
            )
          }) : null}
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <div className="table-opacity">
            <div className="index-opacity">
              <table className="table-valuations table-padding">
                <thead>
                  <tr>
                    <th>Emoción</th>
                    <th>Promedio en el grupo</th>
                    <th>Detalle</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Alegría</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Miedo</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Tristeza</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Sorpresa</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Disgusto</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Otro</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="table-info">
              <img src="/assets/images/temporary/Calendario.png" className="img-calendar" />
              <p className="txt-info">Esta tabla se empezará a llenar con las<br/>respuestas que ingresen tus alumnos</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Create.propTypes = {
  createContent: PropTypes.object.isRequired
};

export default Create;
