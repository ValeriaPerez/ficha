import React,{Component, PropTypes} from 'react';

export class Evaluation extends Component {
  constructor(props, context) {
    super(props, context);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

	render() {
    const content = this.props.evaluationContent;
		return (
      <div>
        {content ? content.map((item, evalu) => {
          return(
            <div className="grey-square" key={evalu}>
              <p dangerouslySetInnerHTML={this.createMarkup(item.evaluation_txt)}></p>
            </div>
          )
        }) : null}
      </div>
		)
	}
}

Evaluation.propTypes = {
  evaluationContent: PropTypes.array.isRequired
};

export default Evaluation;
