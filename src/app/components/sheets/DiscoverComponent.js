import React, {Component, PropTypes} from 'react';

class Discover extends Component {
  constructor(props, context) {
    super(props, context);
  }

  createMarkup(htmlContent) {
    return {__html: htmlContent};
  }

  render() {
    const content = this.props.discoverContent;
    return (
      <div>
        <div className="paragraph-style">
          <img className="img-discover" src={content.image} />
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_student)}></div>
          { (content.question) ?
              (<div className="question">
                <p>
                  <span className="icon-question"></span>
                  <span className="text-question">{content.question}</span>
                </p>
              </div>) : ''
          }
        </div>
        <div className="grey-square">
          <h3>Descubre</h3>
          <div dangerouslySetInnerHTML={this.createMarkup(content.text_teacher)}></div>
          <h3>Información adicional:</h3>
          <p dangerouslySetInnerHTML={this.createMarkup(content.additional_info)}></p>
          <h3>Temas de relevancia social:</h3>
          <p>{content.relevant_themes ? content.relevant_themes.join(', ') : null}</p>
          <h3>Vínculo con:</h3>
          <p>{content.related ? content.related.join(', ') : null}</p>
          <h3>Emoción:</h3>
          <p dangerouslySetInnerHTML={this.createMarkup(content.emotions)}></p>
        </div>
        <div className="reflection">
          <h1>
            <img src="/assets/images/temporary/sheet1.png" />
            <span>¿Qué emociones sintieron<br/>tus alumnos?</span>
          </h1>
          <div className="table-opacity">
            <div className="index-opacity">
              <table className="table-valuations table-padding">
                <thead>
                  <tr>
                    <th>Emoción</th>
                    <th>Promedio en el grupo</th>
                    <th>Detalle</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Alegría</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Miedo</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Tristeza</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Sorpresa</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Disgusto</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                  <tr>
                    <td>Otro</td>
                    <td>
                      <div className="percentage">
                        <div>0 de 30</div>
                        <div>
                          <div className="progress-bar">
                            <div className="progress" style={{width: '0%'}}></div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <i className="fa fa-eye" aria-hidden="true"></i>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="table-info">
              <img src="/assets/images/temporary/Calendario.png" className="img-calendar" />
              <p className="txt-info">Esta tabla se empezará a llenar con las<br/>respuestas que ingresen tus alumnos</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Discover.propTypes = {
  discoverContent: PropTypes.object.isRequired
};

export default Discover;
