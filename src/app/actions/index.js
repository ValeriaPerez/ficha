import * as types from '../constants/ActionTypes';
import * as endpoint from '../constants/EndPoints';
import axios from 'axios';

export function addTodo(text) {
  return {type: types.ADD_TODO, text};
}

export function deleteTodo(id) {
  return {type: types.DELETE_TODO, id};
}

export function editTodo(id, text) {
  return {type: types.EDIT_TODO, id, text};
}

export function completeTodo(id) {
  return {type: types.COMPLETE_TODO, id};
}

export function completeAll() {
  return {type: types.COMPLETE_ALL};
}

export function clearCompleted() {
  return {type: types.CLEAR_COMPLETED};
}

/**
 * Fetch sheet content from end-point
 * @method fetchSheet
 * @param  {string}   sheet Sheet Id
 * @return {function} Dispatch action
 */
export function fetchSheet(sheet) {
  return (dispatch, getState) => {
    dispatch({
      type: types.FETCH_SHEET.INITIAL,
      payload: axios.get(endpoint.GET_DATA_FOR.POTENCIA_FILES + '/' + sheet)
    });
  };
}

/**
 * Fetch hexagons content from end-point
 * @method fetchHexagons
 * @return {function} Dispatch action
 */
export function fetchHexagons() {
  return (dispatch, getState) => {
    const hexagonsState = getState().hexagons;
    dispatch({
      type: types.FETCH_HEXAGONS.INITIAL,
      payload: axios.get(endpoint.GET_DATA_FOR.POTENCIA_FILES, {
        params: hexagonsState.params
      })
    });
  };
}

/**
 * Update the parameters of the search
 * @method updateSearchParams
 * @return {function} Dispatch action
 */
export function updateSearchParams(params) {
  return (dispatch, getState) => {
    dispatch({
      type: types.FETCH_HEXAGONS.UPDATE,
      params
    });
    dispatch(fetchHexagons());
  }
}
